﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Prototipo
{
    
    public class Rueda : ICloneable
    {
        public int DiametroLlantaDelantera { get; set; }
        public int DiametroLlantaTrasera { get; set; }
        public string MaterialLlanta { get; set; }
        public string MarcaNeumatico { get; set; }
        public string MedidaNeumaticoDelantero { get; set; }
        public string MedidaNeumaticoTrasero { get; set; }


        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion ICloneable Members
    }
}