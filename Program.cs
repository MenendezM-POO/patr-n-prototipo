﻿using System;

namespace Patron_Prototipo
{
    

    internal class Program
    {
        private static void Main(string[] args)
        {

            //Crearemos a continuación un nuevo objeto de tipo motocicleta, lo rellenaremos y después lo clonaremos, 


            Motocicleta motocicleta = new Motocicleta();

            motocicleta.Marca = "Harley Davidson";
            motocicleta.Modelo = "Roadster 2016";
            motocicleta.Color = "Negro";

            motocicleta.TipoChasis = new Chasis();
            motocicleta.TipoChasis.Material = " de Acero";
            motocicleta.TipoChasis.TipoChasis = "Tubular";

            motocicleta.TipoRueda = new Rueda();
            motocicleta.TipoRueda.MarcaNeumatico = "Tubeless";
            motocicleta.TipoRueda.MaterialLlanta = "Aluminio";
            motocicleta.TipoRueda.DiametroLlantaDelantera = 18;
            motocicleta.TipoRueda.DiametroLlantaTrasera = 19;
            motocicleta.TipoRueda.MedidaNeumaticoDelantero = "12070R-19 M/C";
            motocicleta.TipoRueda.MedidaNeumaticoTrasero = "15070R-18 M/C";

            //Después debes recorrer todos los campos del objeto original y copiar sus valores en el nuevo objeto.

            //Esto dice, creame un nuevo objeto donde en ese objeto nuevo, se clonaran los datos del primer objeto que es el original.

            Motocicleta motocicletaclonada = motocicleta.Clone() as Motocicleta;
            motocicletaclonada.Color = "gris";
            motocicletaclonada.TipoRueda.MarcaNeumatico = "Dunlop";
            motocicletaclonada.TipoRueda.MaterialLlanta = "de carbono";
            motocicletaclonada.TipoRueda.DiametroLlantaDelantera = 21;
            motocicletaclonada.TipoRueda.DiametroLlantaTrasera = 18;
            motocicletaclonada.TipoRueda.MedidaNeumaticoDelantero = "130/60B21 63H";
            motocicletaclonada.TipoRueda.MedidaNeumaticoTrasero = "240/40R18 79V";


            //En este console mostraremos los datos de los vehiculos, utilizando los objetos e invocando al metodo Info para saber su información.
            Console.WriteLine(motocicleta.MotocicletaInfo());
            Console.WriteLine("--------------------------------------");
            Console.WriteLine(motocicletaclonada.MotocicletaInfo());

            Console.ReadLine();

        }
    }

}

