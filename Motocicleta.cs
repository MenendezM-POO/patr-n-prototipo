﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Patron_Prototipo

{
    // ---------------------------Implementación del patrón-----------------------
    //En.NET este proceso es sencillo, ya que nos ofrece la interfaz ICloneable que expone el método Clone(), 
    //método en el que habrá que codificar el proceso de copia.

    //Además de ofrecer el método Clone(), .NET también ofrece un método, MemberwiseClone(), que automáticamente
    //realiza una copia del objeto por nosotros, evitándonos el proceso de copiar elemento por elemento de forma manual.


    public class Motocicleta : ICloneable
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Color { get; set; }
        public Rueda TipoRueda { get; set; }
        public Chasis TipoChasis { get; set; }

        //la operacion Motocicletainfo va a devolver un mensaje describiendo los datos del vehiculo como  marca, etc.. es  la motocicleta
        public string MotocicletaInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Marca: ").Append(Marca).Append(Environment.NewLine);
            sb.Append("Modelo: ").Append(Modelo).Append(Environment.NewLine);
            sb.Append("Color: ").Append(Color).Append(Environment.NewLine);
            sb.Append("Ruedas: ").Append(TipoRueda.MaterialLlanta).Append(" ").Append(TipoRueda.MarcaNeumatico).Append(" ").Append("Delantera:").Append(" ");
            sb.Append(TipoRueda.DiametroLlantaDelantera).Append(" ").Append(TipoRueda.MedidaNeumaticoDelantero).Append(" ").Append("Trasera:").Append(" ");
            sb.Append(TipoRueda.DiametroLlantaTrasera).Append(" ").Append(TipoRueda.MedidaNeumaticoTrasero).Append(" ").Append(Environment.NewLine);
            sb.Append("Chasis: ").Append(TipoChasis.TipoChasis).Append(" ").Append(TipoChasis.Material).Append(Environment.NewLine);
            return sb.ToString();
        }

        #region ICloneable Members


        //El metodo usado para hacer una clonación superficial es MemberwiseClone();
        //¿que  va a utilizar la llamada menberwiswclone de .net que va a devolver una copia 
        //oficial del objeto en este caso una copia superficial automaticamente.


        public object Clone()
        {
            //Obetenmos una copia superficial del objeto actual.
            object copia = this.MemberwiseClone();



            //Para realizar una clonación profunda la haremos de forma manual. 

            // Recorremos las propiedades del objeto buscando elementos clonables.
            // En caso de encontrar un objeto clonable, realizamos una copia de dicho elemento
            var propiedadesClonables = this.GetType().GetProperties().Where(p => p.PropertyType.GetInterfaces().Contains(typeof(ICloneable)));
            foreach (var propiedad in propiedadesClonables)
            {
                // Obtenemos el nombre de la propiedad (p.e. "TipoRueda")
                var nombrePropiedad = propiedad.Name;

                // Localizamos el método Clone() de la propiedad (TipoRueda.Clone()) y lo
                // invocamos mediante reflection, almacenando el objeto resultante en una variable
                MethodInfo metodoClone = propiedad.PropertyType.GetMethod("Clone");
                var objetoCopia = metodoClone.Invoke(propiedad.GetValue(copia), null);

                // Obtenemos una referencia a la propiedad del objeto clonado (motocicletaclonada.TipoRueda)
                PropertyInfo referenciaCopia = this.GetType().GetProperty(nombrePropiedad, BindingFlags.Public | BindingFlags.Instance);

                // Asignamos el valor del objeto clonado a la referencia (motocicletaclonada.TipoRueda = Rueda2( que esa sera la rueda clonada))
                referenciaCopia.SetValue(copia, objetoCopia, null);
            }

            return copia;
        }

        #endregion ICloneable Members
    }
}
