﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Prototipo
{
   
    public class Chasis : ICloneable
    {
        public string Material { get; set; }
        public string TipoChasis { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
